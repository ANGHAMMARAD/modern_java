package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.stream.Stream;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		LinkedList<NodeD> stack = new LinkedList<>();
		NodeD node = new NodeD(rootDepartment, 1);
		stack.add(node);
		return Stream.iterate(node, x -> x != null, x -> {
			if (stack.isEmpty()) {
				return null;
			}
			NodeD res = stack.pollLast();
			addChildren(res, stack);
			return res;
		}).map(node_dep -> node_dep.level).max(Integer::compareTo).orElse(1);
	}

	private static void addChildren(NodeD node, LinkedList<NodeD> stack) {
		for (Department department : node.department.subDepartments) {
			if (department != null) {
				stack.addLast(new NodeD(department, node.level + 1));
			}
		}
	}

}
