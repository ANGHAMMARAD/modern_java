package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip implements CombatReadyVessel {

	private Ship ship;

	public CombatReadyShip(Ship ship) {
		this.ship = ship;
	}

	@Override
	public void endTurn() {
		this.ship.capacitorAmount = PositiveInteger
				.of(Math.min(this.ship.capacitorAmount.value() + this.ship.capacitorRechargeRate.value(),
						this.ship.maxCapacitorAmount.value()));
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.ship.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.ship.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.ship.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		Optional<AttackAction> result = Optional.empty();
		Integer missing_charge = this.ship.attackSubsystem.getCapacitorConsumption().value()
				- this.ship.capacitorAmount.value();
		if (missing_charge <= 0) {
			result = Optional.of(new AttackAction(this.ship.attackSubsystem.attack(target), this, target,
					this.ship.attackSubsystem));
			this.ship.capacitorAmount = PositiveInteger.of(-missing_charge);
		}
		return result;
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		if (attack.target.getName().equals(this.ship.name)) {
			attack = this.ship.defenciveSubsystem.reduceDamage(attack);
			Integer HP_shield_after_attack = this.ship.shieldHP.value() - attack.damage.value();
			Integer HP_hull_after_attack = this.ship.hullHP.value();
			if (HP_shield_after_attack < 0) {
				HP_hull_after_attack += HP_shield_after_attack;
				this.ship.shieldHP = PositiveInteger.of(0);
			}
			else {
				this.ship.shieldHP = PositiveInteger.of(HP_shield_after_attack);
			}
			if (HP_hull_after_attack <= 0) {
				this.ship.shieldHP = PositiveInteger.of(0);
				return new AttackResult.Destroyed(attack.weapon,
						PositiveInteger.of(attack.damage.value() + HP_hull_after_attack), attack.target);
			}
			else {
				this.ship.hullHP = PositiveInteger.of(HP_hull_after_attack);
			}
		}
		return new AttackResult.DamageRecived(attack.weapon, attack.damage, attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		Optional<RegenerateAction> result = Optional.empty();
		Integer missing_charge = this.ship.defenciveSubsystem.getCapacitorConsumption().value()
				- this.ship.capacitorAmount.value();
		if (missing_charge <= 0) {
			result = Optional
					.of(new RegenerateAction(
							PositiveInteger.of(Math.min(
									this.ship.shieldHP.value()
											+ this.ship.defenciveSubsystem.getShieldRegeneration().value(),
									this.ship.maxShieldHP.value()) - this.ship.shieldHP.value()),
							PositiveInteger.of(Math.min(
									this.ship.hullHP.value()
											+ this.ship.defenciveSubsystem.getHullRegeneration().value(),
									this.ship.maxHullHP.value() - this.ship.hullHP.value()))));
			this.ship.capacitorAmount = PositiveInteger.of(-missing_charge);
		}
		return result;
	}

}
