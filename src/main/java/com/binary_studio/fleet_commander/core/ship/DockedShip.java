package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private Ship ship;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.ship = new Ship(null, null, name, shieldHP, hullHP, powergridOutput, capacitorAmount,
				capacitorRechargeRate, speed, size);
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.ship.attackSubsystem = null;
			return;
		}
		Integer missing_power = this.ship.getDefencivePowerGridConsumption().value()
				+ subsystem.getPowerGridConsumption().value() - this.ship.powergridOutput.value();
		if (missing_power > 0) {
			throw new InsufficientPowergridException(missing_power);
		}
		this.ship.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.ship.defenciveSubsystem = null;
			return;
		}
		Integer missing_power = this.ship.getAttackPowerGridConsumption().value()
				+ subsystem.getPowerGridConsumption().value() - this.ship.powergridOutput.value();
		if (missing_power > 0) {
			throw new InsufficientPowergridException(missing_power);
		}
		this.ship.defenciveSubsystem = subsystem;
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.ship.attackSubsystem != null) {
			if (this.ship.defenciveSubsystem != null) {
				return new CombatReadyShip(this.ship);
			}
			else {
				throw NotAllSubsystemsFitted.defenciveMissing();
			}
		}
		else {
			if (this.ship.defenciveSubsystem != null) {
				throw NotAllSubsystemsFitted.attackMissing();
			}
			else {
				throw NotAllSubsystemsFitted.bothMissing();
			}
		}
	}

}
