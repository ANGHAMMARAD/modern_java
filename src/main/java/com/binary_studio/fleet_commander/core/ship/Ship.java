package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public class Ship {

	public AttackSubsystem attackSubsystem;

	public DefenciveSubsystem defenciveSubsystem;

	public final String name;

	public PositiveInteger shieldHP;

	public PositiveInteger hullHP;

	public final PositiveInteger powergridOutput;

	public PositiveInteger capacitorAmount;

	public final PositiveInteger capacitorRechargeRate;

	public PositiveInteger speed;

	public final PositiveInteger size;

	public final PositiveInteger maxCapacitorAmount;

	public final PositiveInteger maxShieldHP;

	public final PositiveInteger maxHullHP;

	public Ship(AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem, String name,
			PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.maxCapacitorAmount = capacitorAmount;
		this.maxShieldHP = shieldHP;
		this.maxHullHP = hullHP;
	}

	public PositiveInteger getAttackPowerGridConsumption() {
		if (this.attackSubsystem == null) {
			return PositiveInteger.of(0);
		}
		return this.attackSubsystem.getPowerGridConsumption();
	}

	public PositiveInteger getDefencivePowerGridConsumption() {
		if (this.defenciveSubsystem == null) {
			return PositiveInteger.of(0);
		}
		return this.defenciveSubsystem.getPowerGridConsumption();
	}

}
