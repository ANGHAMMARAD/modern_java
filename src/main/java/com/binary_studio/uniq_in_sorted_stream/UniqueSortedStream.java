package com.binary_studio.uniq_in_sorted_stream;

import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		AtomicReference<Long> reference = new AtomicReference<>(null);
		return stream.filter(x -> {
			if (!x.getPrimaryId().equals(reference.get()) || reference.get() == null) {
				reference.set(x.getPrimaryId());
				return true;
			}
			return false;
		});
	}

}
